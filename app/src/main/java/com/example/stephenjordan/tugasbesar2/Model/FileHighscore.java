package com.example.stephenjordan.tugasbesar2.Model;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class FileHighscore {
    protected SharedPreferences sharedPreferences;
    protected ArrayList<Player> player;

    protected String blankName;
    protected int count;


    public FileHighscore(Context context){
        this.sharedPreferences = context.getSharedPreferences("SP_HIGHSCORE",0);
        this.blankName = "NewPlayer";
        this.count = 1;
        loadData();
    }

    public void sorting(){
        Collections.sort(player, new Comparator<Player>() {

            @Override
            public int compare(Player p1, Player p2) {
                return p2.getScore()-p1.getScore();
            }
        });
    }

    public void addNewScore(Player newPlayer){
        if(newPlayer.getName().equalsIgnoreCase("")){
            newPlayer.setName("Player "+this.getCountFromFile());
        }
        setCountFromFile();
        this.player.add(newPlayer);
        this.sorting();
        this.saveData();
    }

    public int getCountFromFile(){
        return sharedPreferences.getInt("COUNT",1);
    }

    public void setCountFromFile(){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("COUNT",getCountFromFile()+1).apply();
    }

    public void saveData() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(player);
        editor.putString("PLAYER",json);
        editor.apply();
    }

    public void loadData(){
        Gson gson = new Gson();
        String json = sharedPreferences.getString("PLAYER",null);
        Type type = new TypeToken<ArrayList<Player>>(){}.getType();

        this.player = gson.fromJson(json,type);
        if(player == null) player = new ArrayList<>();
    }

    public int getBestScore(){
        if (this.player.size()==0)return 0;
        else return this.player.get(0).getScore();
    }


//    public Player getLastPlayer(){
//        if(this.playerScore.size()==0)return null;
//        return this.playerScore.get(this.playerScore.size()-1);
//    }





//    public ArrayList<Player> getArray(){
//        return this.highScore;
//    }
//
//    public int getSize(){
//        return this.highScore.size();
//    }
}
