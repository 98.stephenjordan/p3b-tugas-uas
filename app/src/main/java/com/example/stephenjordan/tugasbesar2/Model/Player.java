package com.example.stephenjordan.tugasbesar2.Model;

public class Player {
    public String name,time;
    public int score;

    public Player(String nama,String time) {
        this.name = nama;
        this.time = "00:00";
        this.score = 0;
    }

    public Player(){
        this.name = "Player";
        this.time = "00:00";
        this.score = 0;
    }

    public void tambahSkor() {
        this.score += 10;
    }

    public String getName() {
        return name;
    }

    public int getScore() {
        return score;
    }

    public String getTime() {
        return time;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
