package com.example.stephenjordan.tugasbesar2.Model;

import java.util.ArrayList;

public class Setting {
    private int backgroundColor,circleColor,circleSize,circleSpeed,difficulty;

    public Setting(int backgroundColor, int circleColor, int circleSize, int circleSpeed, int difficulty) {
        this.backgroundColor = backgroundColor;
        this.circleColor = circleColor;
        this.circleSize = circleSize;
        this.circleSpeed = circleSpeed;
        this.difficulty = difficulty;
    }
}
