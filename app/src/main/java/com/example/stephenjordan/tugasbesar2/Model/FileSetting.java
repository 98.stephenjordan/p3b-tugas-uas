package com.example.stephenjordan.tugasbesar2.Model;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class FileSetting {
    protected SharedPreferences sharedPreferences;
    protected ArrayList<Setting> setting;
    private int backgroundColor,circleColor,circleSize,circleSpeed,difficulty;


    public FileSetting(Context context, int backgroundColor, int circleColor, int circleSize, int circleSpeed, int level ){
        this.sharedPreferences = context.getSharedPreferences("SP_SETTING",0);
        this.backgroundColor = backgroundColor;
        this.circleColor = circleColor;
        this.circleSize = circleSize;
        this.circleSpeed = circleSpeed;
        this.difficulty = level;
        loadData();
    }


    public void addNewSetting(Setting setting){
        this.setting.add(setting);
        this.saveData();
    }


    public void saveData() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(setting);
        editor.putString("PLAYER",json);
        editor.apply();
    }

    public void loadData(){
        Gson gson = new Gson();
        String json = sharedPreferences.getString("SETTING",null);
        Type type = new TypeToken<ArrayList<Player>>(){}.getType();
        this.setting = gson.fromJson(json,type);
        if(setting == null) setting = new ArrayList<>();
    }

    public void setDifficulty(String difficulty) {
        if (difficulty.equalsIgnoreCase("easy")){
            this.difficulty = 2;
        }
        else if(difficulty.equalsIgnoreCase("medium")){
            this.difficulty = 4;
        }
        else{
            this.difficulty = 5;
        }
    }

}
