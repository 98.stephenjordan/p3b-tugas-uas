package com.example.stephenjordan.tugasbesar2.View;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.example.stephenjordan.tugasbesar2.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentHighscore extends Fragment implements View.OnClickListener{
    private Button btnExit,btnAdd;
    private ListenerFragmentHighscore listener;

    //List View
    private ListView lvHighscore;
    private ListViewAdapter adapter;

    public FragmentHighscore() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View fragmentHighscore = inflater.inflate(R.layout.fragment_highscore,container,false);

        this.lvHighscore=fragmentHighscore.findViewById(R.id.lst_item);
        this.adapter=new ListViewAdapter(this);
        this.lvHighscore.setAdapter(this.adapter);

        this.btnExit = fragmentHighscore.findViewById(R.id.btn_exit_highscore);
        this.btnAdd = fragmentHighscore.findViewById(R.id.btn_add);
        this.btnAdd.setOnClickListener(this);
        this.btnExit.setOnClickListener(this);

        return fragmentHighscore;
    }

    public static FragmentHighscore newInstance(ListenerFragmentHighscore listener){
        FragmentHighscore fragmentHighscore = new FragmentHighscore();
        fragmentHighscore.listener=listener;
        return fragmentHighscore;
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==this.btnExit.getId()){
            listener.pindahFrag("FragmentHome");
        }
        else if(v.getId()==this.btnAdd.getId()){
            this.adapter.addLine("Player 1","1000pt","01:00");
        }
    }

}
