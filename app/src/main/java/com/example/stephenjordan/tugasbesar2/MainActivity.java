package com.example.stephenjordan.tugasbesar2;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.stephenjordan.tugasbesar2.View.FragmentGame;
import com.example.stephenjordan.tugasbesar2.View.FragmentHighscore;
import com.example.stephenjordan.tugasbesar2.View.FragmentHome;
import com.example.stephenjordan.tugasbesar2.View.FragmentSetting;
import com.example.stephenjordan.tugasbesar2.View.ListenerFragmentGame;
import com.example.stephenjordan.tugasbesar2.View.ListenerFragmentHighscore;
import com.example.stephenjordan.tugasbesar2.View.ListenerFragmentHome;
import com.example.stephenjordan.tugasbesar2.View.ListenerFragmentSetting;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,ListenerFragmentGame,ListenerFragmentHighscore,ListenerFragmentHome,ListenerFragmentSetting {
    private FragmentGame fragmentGame;
    private FragmentHome fragmentHome;
    private FragmentHighscore fragmentHighscore;
    private FragmentSetting fragmentSetting;

    private Button btnNew,btnExit;

    private LinearLayout mLayout;
    private int mDefaultColor;
    private Button btnColorPicker;



    DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.initiateNavigationDrawer(savedInstanceState);

        this.pindahFrag("FragmentHome");
    }

    //Navigation Drawer
        public void initiateNavigationDrawer(Bundle savedInstanceState){
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            drawer = findViewById(R.id.drawer_layout);
            NavigationView navigationView = findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);

            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                    R.string.navigation_open, R.string.navigation_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();
            if(savedInstanceState==null){
                getSupportFragmentManager().beginTransaction().replace(R.id.fragContainer,new FragmentHighscore()).commit();
                navigationView.setCheckedItem(R.id.highscore);
            }
        }

        @Override
        public void onBackPressed() {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
            }
        }

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            switch(menuItem.getItemId()){
                case R.id.highscore:
                    pindahFrag("FragmentHighscore");
                    break;
                case R.id.settings:
                    pindahFrag("FragmentSetting");
                    break;
                case R.id.exit:
                    Toast.makeText(this,"EXIT",Toast.LENGTH_SHORT).show();
                    super.onBackPressed();
            }
            drawer.closeDrawer(GravityCompat.START);
            return true;
        }

    //Fragment
        @Override
        public void pindahFrag(String fragment) {
            if(fragment.equalsIgnoreCase("FragmentGame")){
                this.fragmentGame = FragmentGame.newInstance(this);
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragContainer,this.fragmentGame);
                transaction.commit();
            }
            else if(fragment.equalsIgnoreCase("FragmentHighscore")){
                this.fragmentHighscore = FragmentHighscore.newInstance(this);
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragContainer,this.fragmentHighscore);
                transaction.commit();
            }
            else if(fragment.equalsIgnoreCase("FragmentHome")){
                this.fragmentHome = FragmentHome.newInstance(this);
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragContainer,this.fragmentHome);
                transaction.commit();
            }
            else if(fragment.equalsIgnoreCase("FragmentSetting")){
                this.fragmentSetting = FragmentSetting.newInstance(this);
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragContainer,this.fragmentSetting);
                transaction.commit();
            }
        }
}
