package com.example.stephenjordan.tugasbesar2.View;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.stephenjordan.tugasbesar2.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentHome extends Fragment implements View.OnClickListener {
    private Button btnNew,btnExit;
    private ListenerFragmentHome listener;

    public FragmentHome() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentHome = inflater.inflate(R.layout.fragment_home,container,false);
        this.btnNew = fragmentHome.findViewById(R.id.btn_new);
        this.btnExit = fragmentHome.findViewById(R.id.btn_exit);
        this.btnNew.setOnClickListener(this);
        this.btnExit.setOnClickListener(this);
        return fragmentHome;
    }

    public static FragmentHome newInstance(ListenerFragmentHome listener){
        FragmentHome fragmentHome = new FragmentHome();
        fragmentHome.listener=listener;
        return fragmentHome;
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==this.btnNew.getId()){
            this.listener.pindahFrag("FragmentGame");
        }
        else if(v.getId()==this.btnExit.getId()){
            System.exit(0);
        }
    }
}
