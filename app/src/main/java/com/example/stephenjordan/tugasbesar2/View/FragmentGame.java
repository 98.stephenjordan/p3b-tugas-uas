package com.example.stephenjordan.tugasbesar2.View;


import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.stephenjordan.tugasbesar2.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentGame extends Fragment implements View.OnClickListener{


    private Button btnNew,btnExit;

    private ListenerFragmentGame listener;

    private CountDownTimer timer;
    private TextView tvTime;

    public FragmentGame() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentGame = inflater.inflate(R.layout.fragment_game,container,false);
        this.btnNew = fragmentGame.findViewById(R.id.btn_new);
        this.btnExit = fragmentGame.findViewById(R.id.btn_exit_game);
        this.tvTime = fragmentGame.findViewById(R.id.tv_time);
        this.btnNew.setOnClickListener(this);
        this.btnExit.setOnClickListener(this);
        return fragmentGame;
    }

    public static FragmentGame newInstance(ListenerFragmentGame listener){
        FragmentGame fragmentGame = new FragmentGame();
        fragmentGame.listener = listener;
        return fragmentGame;
    }

    @Override
    public void onClick(View v) {
        if (v.getId()==this.btnNew.getId()){
            this.listener.pindahFrag("FragmentGame");
        }
        else if(v.getId()==this.btnExit.getId()){
            this.listener.pindahFrag("FragmentHome");
        }
    }

    private void startTimer() {
        this.timer = new CountDownTimer(60000,1000) {
            @Override
            public void onTick(long l) {
                int second = Integer.parseInt(tvTime.getText()+"");
                if(second%10==0){

                }
                else if(second%60==0){

                }
                tvTime.setText("Time : " + l/1000 + "s");
            }

            @Override
            public void onFinish() {
                clearPetunjuk();
                mCanvas.drawColor(getResources().getColor(R.color.white));
                canvasInitiated=false;
                mDetector=null;
                p.setSkor(scoreTemp );
                this.showPopup();
            }
        }.start();
    }
}
