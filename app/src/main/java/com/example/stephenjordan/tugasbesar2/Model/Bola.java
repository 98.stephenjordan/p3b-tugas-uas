package com.example.stephenjordan.tugasbesar2.Model;

public class Bola {
    private int cx,cy,r;
    private float bobot;

    public Bola(int cx, int cy, int r, float bobot) {
        this.cx = cx;
        this.cy = cy;
        this.r = r;
        this.bobot = bobot;
    }

    //Getter
    public int getCx() {
        return cx;
    }

    public int getCy() {
        return cy;
    }

    public int getR() {
        return r;
    }

    public float getBobot() {
        return bobot;
    }

    //Setter
    public void setCx(int cx) {
        this.cx = cx;
    }

    public void setCy(int cy) {
        this.cy = cy;
    }

    public void setR(int r) {
        this.r = r;
    }

    public void setBobot(float bobot) {
        this.bobot = bobot;
    }
}
