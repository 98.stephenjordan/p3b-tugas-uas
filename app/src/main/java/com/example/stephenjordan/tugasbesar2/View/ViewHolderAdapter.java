package com.example.stephenjordan.tugasbesar2.View;

import android.widget.TextView;

public class ViewHolderAdapter {
    private TextView tvNama,tvPoint,tvTime;
    private int dataPosition;

    public ViewHolderAdapter(TextView tvNama, TextView tvPoint, TextView tvTime, int dataPosition) {
        this.tvNama = tvNama;
        this.tvPoint = tvPoint;
        this.tvTime = tvTime;
        this.dataPosition = dataPosition;
    }

    //Getter
        public TextView getTvNama() {
            return tvNama;
        }

        public TextView getTvPoint() {
            return tvPoint;
        }

        public TextView getTvTime() {
            return tvTime;
        }

        public int getDataPosition() {
            return dataPosition;
        }

    //Setter
        public void setTvNama(TextView tvNama) {
            this.tvNama = tvNama;
        }

        public void setTvPoint(TextView tvPoint) {
            this.tvPoint = tvPoint;
        }

        public void setTvTime(TextView tvTime) {
            this.tvTime = tvTime;
        }

        public void setDataPosition(int dataPosition) {
            this.dataPosition = dataPosition;
        }
}
