package com.example.stephenjordan.tugasbesar2.View;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.stephenjordan.tugasbesar2.R;

import java.util.ArrayList;

public class ListViewAdapter extends BaseAdapter {
    private ArrayList<String> listNama,listPoint,listTime;
    private LayoutInflater inflater;

    public ListViewAdapter(FragmentHighscore fragmentHighscore) {
        this.listNama = new ArrayList<>();
        this.listPoint = new ArrayList<>();
        this.listTime = new ArrayList<>();
        this.inflater = fragmentHighscore.getLayoutInflater();
    }

    @Override
    public int getCount() {
        return this.listNama.size();
    }

    @Override
    public Object getItem(int position) {
        return this.listNama.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View ListView;
        TextView tvNama;
        TextView tvPoint;
        TextView tvTime;

        if(convertView==null){
            ListView=this.inflater.inflate(R.layout.listview_highscore,null); //(a)
            tvNama=ListView.findViewById(R.id.tv_nama);
            tvPoint=ListView.findViewById(R.id.tv_point);
            tvTime=ListView.findViewById(R.id.tv_time);
        }
        else{
            ListView=  convertView;
            tvNama = ((ViewHolderAdapter) ListView.getTag()).getTvNama();
            tvPoint = ((ViewHolderAdapter) ListView.getTag()).getTvPoint();
            tvTime = ((ViewHolderAdapter) ListView.getTag()).getTvTime();
        }

        ViewHolderAdapter ViewHolder=new ViewHolderAdapter(tvNama,tvPoint,tvTime,position);
        ListView.setTag(ViewHolder);

        tvNama.setText(this.listNama.get(position));
        tvPoint.setText(this.listPoint.get(position));
        tvTime.setText(this.listTime.get(position));


        return ListView;
    }

    public void addLine(String nama,String point, String time){
        this.listNama.add(nama);
        this.listPoint.add(point);
        this.listTime.add(time);
        this.notifyDataSetChanged();
    }
}
