package com.example.stephenjordan.tugasbesar2.View;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.stephenjordan.tugasbesar2.R;

import yuku.ambilwarna.AmbilWarnaDialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentSetting extends Fragment implements View.OnClickListener {
    private LinearLayout mLayout;
    private int mDefaultColor;
    private Button btnColorPicker,btnExit;

    private ListenerFragmentSetting listener;



    public FragmentSetting() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentSetting = inflater.inflate(R.layout.fragment_setting, container, false);
        this.mLayout=fragmentSetting.findViewById(R.id.layout);
        this.mDefaultColor = ResourcesCompat.getColor(getResources(), R.color.colorPrimary, null);
        this.btnColorPicker = fragmentSetting.findViewById(R.id.btnColorPicker);
        this.btnExit = fragmentSetting.findViewById(R.id.btn_exit_setting);
        this.btnColorPicker.setOnClickListener(this);
        this.btnExit.setOnClickListener(this);
        return fragmentSetting;
    }

    public static FragmentSetting newInstance(ListenerFragmentSetting listener){
        FragmentSetting fragmentSetting = new FragmentSetting();
        fragmentSetting.listener = listener;
        return fragmentSetting;
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==this.btnColorPicker.getId()){
            this.openColorPicker();
        }
        else if(v.getId()==this.btnExit.getId()){
            this.listener.pindahFrag("FragmentHome");
        }
    }

    public void openColorPicker(){
        AmbilWarnaDialog colorPicker = new AmbilWarnaDialog(getActivity(), mDefaultColor, new AmbilWarnaDialog.OnAmbilWarnaListener() {
            @Override
            public void onCancel(AmbilWarnaDialog dialog) {

            }

            @Override
            public void onOk(AmbilWarnaDialog dialog, int color) {
                mDefaultColor = color;
                mLayout.setBackgroundColor(mDefaultColor);
            }
        });
        colorPicker.show();
    }
}
